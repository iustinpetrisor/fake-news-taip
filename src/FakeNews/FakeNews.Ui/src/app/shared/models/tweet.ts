import { User } from './user';
import { Entities } from './entities';

export class Tweet {
  createdAt: string;
  id: number;
  text: string;
  source: string;
  user: User;
  retweet_count: number;
  favorite_count: number;
  score: number;
  entities: Entities;
}
