﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FakeNews.TwitterModels.Models
{
    public class ClassifiedTweet
    {
        public static ClassifiedTweet FromJson(string json) => JsonConvert.DeserializeObject<ClassifiedTweet>(json, Converter.Settings);

        [JsonProperty("tweet")]
        public Tweet Tweet { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }
    }
}