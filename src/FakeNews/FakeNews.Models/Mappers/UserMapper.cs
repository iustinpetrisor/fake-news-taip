﻿using System;
using FakeNews.Database;
using FakeNews.TwitterModels.Convertors;
using FakeNews.TwitterModels.Models;

namespace FakeNews.TwitterModels.Mappers
{
    public static class UserMapper
    {
        public static UserDb ToDb(this User user)
        {
            var userDb = new UserDb()
            {
                Id = user.Id,
                CreatedAt = user.CreatedAt?.ToDateTime() ?? DateTime.MinValue,
                Url = user.Url?.AbsoluteUri,
                Lang = user.Lang,
                Description = user.Description,
                Name = user.Name,
                FollowersCount = user.FollowersCount,
                FavouritesCount = user.FavouritesCount,
                FriendsCount = user.FriendsCount,
                ListedCount = user.ListedCount,
                Location = user.Location,
                ScreenName = user.ScreenName,
                StatusesCount = user.StatusesCount,
                Verified = user.Verified
            };
            return userDb;
        }


        public static User ToBusinessLogicModel(this UserDb userDb)
        {
            var user = new User
            {
                Id = userDb.Id,
                CreatedAt = userDb.CreatedAt.ToString(),
                Lang = userDb.Lang,
                Description = userDb.Description,
                Name = userDb.Name,
                FollowersCount = userDb.FollowersCount,
                FavouritesCount = userDb.FavouritesCount,
                FriendsCount = userDb.FriendsCount,
                ListedCount = userDb.ListedCount,
                Location = userDb.Location,
                ScreenName = userDb.ScreenName,
                StatusesCount = userDb.StatusesCount,
                Verified = userDb.Verified
            };

            return user;
        }
    }
}
