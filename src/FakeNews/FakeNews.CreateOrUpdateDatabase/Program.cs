﻿using System;
using System.Globalization;
using FakeNews.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FakeNews.CreateOrUpdateDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            //var env = FormatEnvironment(args[0]);

            var builder = new ConfigurationBuilder()
                .SetBasePath(
                    System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                //.AddJsonFile($"appsettings.{env}.json", optional: false, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();
            var optionsBuilder = new DbContextOptionsBuilder<FakeNewsContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));

            var context = new FakeNewsContext(optionsBuilder.Options);
            context.Database.Migrate();
        }
        private static string FormatEnvironment(string environment)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(environment);
        }
    }
}
