﻿using System;
using System.Collections.Generic;
using System.Text;
using FakeNews.Database;
using FakeNews.TwitterModels.Convertors;
using FakeNews.TwitterModels.Mappers;
using FakeNews.TwitterModels.Models;
using Xunit;

namespace FakeNews.TwitterModels.Test
{
    public class ConvertorTest
    {
        [Fact]
        public void ConvertDateStringToDateTime()
        {
            string dateTimeString = "Sun Feb 25 18:11:01 +0000 2018";
            DateTime dateTime = dateTimeString.ToDateTime();
            Assert.Equal(2018, dateTime.Year);
        }
    }
}
