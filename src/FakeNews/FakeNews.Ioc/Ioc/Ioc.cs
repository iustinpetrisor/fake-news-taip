﻿using Autofac;
using Autofac.Builder;
using Autofac.Extras.DynamicProxy;
using FakeNews.BusinessLogic;
using FakeNews.Database.Repositories;
using FakeNews.Ioc.Interceptors;
using FakeNews.Ioc.Logger;
using FakeNews.Utils;
using Microsoft.Extensions.DependencyInjection;

namespace FakeNews.Ioc.Ioc
{
    public static class Ioc
    {
        public static void RegisterServices(this ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterInstance(new FakeNewsLogger());
            containerBuilder.RegisterType(typeof(CacheResultInterceptor)).AsSelf();
            containerBuilder.RegisterType<MemoryCacheProvider>().As<ICacheProvider>();

            containerBuilder.RegisterWithCache<TweetRepository, ITweetRepository>().InstancePerLifetimeScope();

            containerBuilder.RegisterWithLogger<TweetService, ITweetService>();
        }


        public static void RegisterServicesForApi(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ITweetRepository, TweetRepository>();
            serviceCollection.AddScoped<ITweetService, TweetService>();
        }

        public static IRegistrationBuilder<TInterface, ConcreteReflectionActivatorData, SingleRegistrationStyle> RegisterWithLogger<TInterface, TConcretType>(this ContainerBuilder containerBuilder)
        {
           return containerBuilder.RegisterType<TInterface>().As<TConcretType>().EnableInterfaceInterceptors().InterceptedBy(typeof(FakeNewsLogger));
        }

        public static IRegistrationBuilder<TInterface, ConcreteReflectionActivatorData, SingleRegistrationStyle> RegisterWithCache<TInterface, TConcretType>(this ContainerBuilder containerBuilder)
        {
            return containerBuilder.RegisterType<TInterface>().As<TConcretType>().EnableInterfaceInterceptors().InterceptedBy(typeof(CacheResultInterceptor));
        }
    }
}
