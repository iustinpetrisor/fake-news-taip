import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Tweet } from '../shared/models/tweet';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = `${environment.apiEndpoint}/Tweets`;

@Injectable()
export class TweetService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = API_URL;
  }

  getAllTweets(): Observable<Tweet[]> {
    return this.http.get<Tweet[]>(this.url + '/tweets');
  }
}
