﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FakeNews.TwitterModels.Convertors
{
    public static class DateTimeConvertor
    {
        public const string ConstTwitterDateTemplate = "ddd MMM dd HH:mm:ss +ffff yyyy";
        public static DateTime ToDateTime(this string dateTimeString)
        {
            return DateTime.ParseExact(dateTimeString,
                ConstTwitterDateTemplate, new System.Globalization.CultureInfo("en-US"));
        }
    }
}
