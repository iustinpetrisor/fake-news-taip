﻿using Newtonsoft.Json;

namespace FakeNews.TwitterModels.Models
{
    public partial class Metadata
    {
        [JsonProperty("result_type")]
        public string ResultType { get; set; }

        [JsonProperty("iso_language_code")]
        public string IsoLanguageCode { get; set; }
    }
}