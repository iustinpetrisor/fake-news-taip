﻿using System.Collections.Generic;

namespace FakeNews.Crawler
{
    public interface ICrawlerBuilder
    {
        Crawler Build();
        CrawlerBuilder SetAccessToken(string accessToken);
        CrawlerBuilder SetAccessTokenSecret(string accessTokenSecret);
        CrawlerBuilder SetConsumerKey(string consumerKey);
        CrawlerBuilder SetConsumerSecert(string consumerSecret);
        CrawlerBuilder SetSearchConfig(Dictionary<string, string> searchConfig);
        CrawlerBuilder SetUrl(string url);
    }
}