# pandas a library for data structures and data analysis tools for the Python programming language
import pandas as pd

# toolbox for imbalanced dataset in machine learning.
from imblearn.over_sampling import SMOTE

# function used to count the size of each class
from collections import Counter

# transform data set in vocabulary using term frequency inverse document frequency metric
from sklearn.feature_extraction.text import TfidfVectorizer

# compute precision, recall, fscore, support for the results given by each classifier
from sklearn.metrics import precision_recall_fscore_support

# get classification results report
from sklearn.metrics import classification_report

# library for linear algebra
import numpy as np

# function to split data set into train, test
from sklearn.model_selection import train_test_split

# Gaussian Process Classifier: http://www.gaussianprocess.org/gpml/chapters/RW3.pdf
from sklearn.gaussian_process import GaussianProcessClassifier
# Kernel for Gaussian Process Classifier
from sklearn.gaussian_process.kernels import RBF

# https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
from sklearn.ensemble import RandomForestClassifier

# https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.GaussianNB.html
from sklearn.naive_bayes import GaussianNB


import glob
path =r'data for invoice' # use your path
allFiles = glob.glob(path + "/*.csv")
frame = pd.DataFrame()
list_ = []
for file_ in allFiles:
    df = pd.read_csv(file_,index_col=None, header=0)
    list_.append(df)
frame = pd.concat(list_)


# lower the strings from name columns
frame['Name'] = frame['Name'].str.lower()
# merge column name with column description
print(frame.head())
frame = frame.sample(frac=1).reset_index(drop=True)
# transform description into a vectorized version; a step that is required to transform
# the words into numerical representation and the frequency within one document and between

from sklearn import preprocessing
from sklearn.feature_extraction.text import CountVectorizer
input = frame["Name"]
vectorizer = CountVectorizer()
X = vectorizer.fit_transform(input)
print(pd.DataFrame(X.toarray()))

X = pd.DataFrame(X.toarray())
eightyPercent = int(len(frame.index)*0.8)
twentyPercent = int(len(frame.index)*0.2)


# y is the label for our classification
y = pd.DataFrame(frame['Class'])
print('Original dataset shape {}'.format(Counter(y)))
# split the data set into training set and test set; 80-20 ratio
frame = pd.DataFrame([X, y])
frame = frame.sample(frac=1).reset_index(drop=True)
print(frame.head())

X_train = X[: eightyPercent]
X_test = X[eightyPercent:]

y_train = y[:eightyPercent]
y_test = y[eightyPercent:]

#
# # random forest classifier
# # model with 100 decision trees with criterion entropy, related to information gain
# # https://bricaud.github.io/personal-blog/entropy-in-decision-trees/
# # https://towardsdatascience.com/the-random-forest-algorithm-d457d499ffcd
from sklearn.metrics import accuracy_score

# from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
# clf_LinearDiscriminantAnalysis = LinearDiscriminantAnalysis(solver='svd')
# clf_LinearDiscriminantAnalysis = clf_LinearDiscriminantAnalysis.fit(X_train, y_train.values.ravel())
# prediction_LinearDiscriminantAnalysis = clf_LinearDiscriminantAnalysis.predict(np.matrix(X_test))
# accuracy_LinearDiscriminantAnalysis = accuracy_score(prediction_LinearDiscriminantAnalysis, y_test.values.ravel())
# print('accuracy_LinearDiscriminantAnalysis', accuracy_LinearDiscriminantAnalysis)


# from sklearn.svm import SVC, LinearSVC, NuSVC
# clf_SVC = SVC(kernel="sigmoid", probability=True)
# clf_SVC = clf_SVC.fit(X_train, y_train.values.ravel())
# prediction_SVC = clf_SVC.predict(np.matrix(X_test))
# accuracy_SVC = accuracy_score(prediction_SVC, y_test.values.ravel())
# print('accuracy_SVC', accuracy_SVC)
#
# clf_NuSVC = NuSVC(gamma='scale', nu=0.02, degree=10, probability=True)
# clf_NuSVC = clf_NuSVC.fit(X_train, y_train.values.ravel())
# prediction_NuSVC = clf_NuSVC.predict(np.matrix(X_test))
# accuracy_NuSVC = accuracy_score(y_test, prediction_NuSVC)
# print('accuracy_NuSVC', accuracy_NuSVC)
#
#
# clf_RandomForestClassifier = RandomForestClassifier(n_estimators=10, criterion="entropy")
# clf_RandomForestClassifier = clf_RandomForestClassifier.fit(X_train, y_train.values.ravel())
# prediction_RandomForestClassifier = clf_RandomForestClassifier.predict(np.matrix(X_test))
# accuracy_RandomForestClassifier = accuracy_score(prediction_RandomForestClassifier, y_test.values.ravel())
# print('accuracy_RandomForestClassifier',accuracy_RandomForestClassifier)
# precision_recall_fscore_support_RandomForestClassifier = precision_recall_fscore_support(y_test, prediction_RandomForestClassifier)
# print(precision_recall_fscore_support(y_test, prediction_RandomForestClassifier))
# print(classification_report(y_test, prediction_RandomForestClassifier))
#
# # # fit the multinomial naive bayes classifier to the data
# # # https://towardsdatascience.com/multinomial-naive-bayes-classifier-for-text-analysis-python-8dd6825ece67
from sklearn.naive_bayes import MultinomialNB
clf_MultinomialNB = MultinomialNB(alpha=0.5).fit(X_train,  y_train.values.ravel())
prediction_MultinomialNB = clf_MultinomialNB.predict(np.matrix(X_test))
accuracy_MultinomialNB = accuracy_score( y_test, prediction_MultinomialNB)
print('accuracy_MultinomialNB',accuracy_MultinomialNB)
print('probabilities ' , clf_MultinomialNB.predict_proba(np.matrix(X_test)))
# print(precision_recall_fscore_support(y_test, prediction_MultinomialNB))
# print(classification_report(y_test, prediction_MultinomialNB))

from sklearn.metrics import confusion_matrix
labels = np.unique(y_test)
print(labels)
cm = confusion_matrix(y_test, prediction_MultinomialNB, labels=np.unique(y_test))  # create the confusion matrix
print(cm)
print(cm.sum())
import itertools
accuracy_per_class = []
sizes = []

print(accuracy_per_class)
for i in range (0, len(labels)):
    print(i, labels[i])
    column = cm[:,i]
    row = cm[i,:]
    column= [x for j, x in enumerate(column) if j != i]
    row= [x for j, x in enumerate(row) if j != i]
    print(column)
    print(row)
    tp = cm[i,i]
    fn = sum(column)
    fp = sum(row)
    submatrix = cm
    submatrix = np.delete(submatrix, i, 0)
    submatrix = np.delete(submatrix, i, 1)

    # submatrix = np.delete(submatrix, 0, i)
    print("submatrix: ", submatrix)
    tn = submatrix.sum()
    print("tn ", tn, " fn  ", fn, " fp ", fp, " tp ", tp)
    accuracy_i = (tp + tn)/ (tp+tn+fp+fn)
    print("accuracy_i: ",accuracy_i)
    accuracy_per_class.append(accuracy_i)
    print(accuracy_per_class[i])
    sizes.append(tp)

for i in range(0, len(labels)-1):
    print(labels[i], " ", sizes[i], " ", accuracy_per_class[i])
import json


labels = list(labels)
# sizes = np.array(sizes)
print(type(sizes), len(sizes))
print(type(labels), len(labels))

accuracy = list(accuracy_per_class)
# lists = ['labels', 'sizes', 'accuracy_per_class']

# data = {listname: globals()[listname] for listname in lists}
data =  pd.DataFrame(
    {'labels': labels,
     'sizes': sizes,
     'accuracy_per_class': accuracy_per_class
    }).to_json()

with open('result.json', 'w') as outfile:
    json.dump(data, outfile)

#
# from sklearn.ensemble import GradientBoostingClassifier
# clf_GradientBoostingClassifier = GradientBoostingClassifier(n_estimators=100, learning_rate=0.5,
#  max_depth=2, random_state=0).fit(X_train,  y_train.values.ravel())
# prediction_GradientBoostingClassifier = clf_GradientBoostingClassifier.predict(np.matrix(X_test))
# accuracy_GradientBoostingClassifier = accuracy_score( y_test, prediction_GradientBoostingClassifier)
# print(accuracy_GradientBoostingClassifier)
# #
# from sklearn.naive_bayes import GaussianNB
# clf_GaussianNB = GaussianNB().fit(X_train,  y_train.values.ravel())
# prediction_GaussianNB = clf_GaussianNB.predict(np.matrix(X_test))
# accuracy_GaussianNB = accuracy_score(y_test, prediction_GaussianNB)
# print('accuracy_GaussianNB',accuracy_GaussianNB)
# print(precision_recall_fscore_support(y_test, prediction_GaussianNB))
# print(classification_report(y_test, prediction_GaussianNB))
# #
# from sklearn.naive_bayes import ComplementNB
# clf_ComplementNB = ComplementNB(alpha=0.7).fit(X_train,  y_train.values.ravel())
# prediction_ComplementNB = clf_ComplementNB.predict(np.matrix(X_test))
# accuracy_ComplementNB = accuracy_score(y_test, prediction_ComplementNB)
# print('accuracy_ComplementNB',accuracy_ComplementNB)
# print(precision_recall_fscore_support(y_test, prediction_ComplementNB))
# print(classification_report(y_test, prediction_ComplementNB))


