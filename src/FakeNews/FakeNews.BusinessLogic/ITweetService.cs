﻿using System.Collections.Generic;
using FakeNews.Database;
using FakeNews.TwitterModels.Models;

namespace FakeNews.BusinessLogic
{
    public interface ITweetService
    {
        void SaveTweets(IEnumerable<Tweet> tweets);

        IEnumerable<Tweet> GetTweets();

        IEnumerable<User> GetUsers();
        IEnumerable<TweetDb> Get();
        Tweet Get(long id);
    }
}