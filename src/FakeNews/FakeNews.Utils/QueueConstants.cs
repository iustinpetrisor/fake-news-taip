﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FakeNews.Utils
{
    public static class QueueConstants
    {
        public const string QueueTweets = "queue-tweets";
        public const string QueueUsers = "queue-users";

        public const string QueueTweetsClassified = "queue-tweets-classified";
        public const string QueueUsersClassified = "queue-users-classified";

        public const string Exchange = "queue-exchange";
        public const string ExchangeClassified = "queue-exchange-classified";
    }
}
