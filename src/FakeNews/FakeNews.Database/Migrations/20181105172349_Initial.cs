﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FakeNews.Database.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ScreenName = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    FollowersCount = table.Column<long>(nullable: true),
                    FriendsCount = table.Column<long>(nullable: true),
                    ListedCount = table.Column<long>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    FavouritesCount = table.Column<long>(nullable: true),
                    Verified = table.Column<bool>(nullable: false),
                    StatusesCount = table.Column<long>(nullable: true),
                    Lang = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tweet",
                columns: table => new
                {
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Id = table.Column<long>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    HashTags = table.Column<string>(nullable: true),
                    Symbols = table.Column<string>(nullable: true),
                    ResultType = table.Column<string>(nullable: true),
                    IsoLanguageCode = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    RetweetCount = table.Column<long>(nullable: true),
                    FavoriteCount = table.Column<long>(nullable: true),
                    Favorited = table.Column<bool>(nullable: false),
                    Retweeted = table.Column<bool>(nullable: false),
                    PossiblySensitive = table.Column<bool>(nullable: false),
                    Lang = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tweet", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tweet_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tweet_UserId",
                table: "Tweet",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tweet");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
