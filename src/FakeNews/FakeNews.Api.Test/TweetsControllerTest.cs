using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using FakeNews.Api.Controllers;
using FakeNews.BusinessLogic;
using FakeNews.Database;
using FakeNews.TwitterModels.Models;
using Moq;
using Xunit;

namespace FakeNews.Api.Test
{
    public class TweetsControllerTest
    {
        private readonly Mock<ITweetService> _mockTweetService = new Mock<ITweetService>();
        private readonly IEnumerable<Tweet> _listTweets = new List<Tweet>
        {
            new Tweet
            {
                Id = 3,
                Text = "Test tweet 3"
            },
            new Tweet
            {
                Id = 4,
                Text = "Test tweet 4"
            },
            new Tweet
            {
                Id = 5,
                Text = "Test tweet 5"
            }
        };

        public TweetsControllerTest()
        {
            _mockTweetService.Setup(x => x.GetTweets()).Returns(_listTweets);
            _mockTweetService.Setup(x => x.Get(It.IsAny<long>())).Returns((long i) => _listTweets.Single(x => x.Id == i));
        }

        [Fact]
        public void GetReturnsTweets()
        {
            // Arrange
            var controller = new TweetsController(_mockTweetService.Object);
            // Act
            var result = controller.Get();
            // Assert
            Assert.IsAssignableFrom<IEnumerable<Tweet>>(result);
        }

        [Fact]
        public void ReturnsTweetById()
        {
            // Arrange
            var controller = new TweetsController(_mockTweetService.Object);
            // Act
            var result = controller.Get(3);

            Assert.NotNull(result); // Test if null
            Assert.IsType<Tweet>(result); // Test type
            Assert.Equal("Test tweet 3", result.Text); // Verify it is the right tweet
        }

        [Fact]
        public void ReturnsAllTweets()
        {
            // Arrange
            var controller = new TweetsController(_mockTweetService.Object);
            // Act
            var result = controller.Get();
            // Assert
            // Assert
            Assert.IsAssignableFrom<IEnumerable<Tweet>>(result);
            Assert.Equal(3, result.Count()); // Verify the correct Number
        }
    }
}
