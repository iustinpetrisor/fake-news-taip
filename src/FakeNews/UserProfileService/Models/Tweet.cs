﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserProfilerService.Models
{
    public class Tweet
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public IEnumerable<User> UserLikes { get; set; }
    }
}
