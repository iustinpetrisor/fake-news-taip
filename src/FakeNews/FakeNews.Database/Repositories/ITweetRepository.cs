﻿using System.Collections.Generic;
using FakeNews.Utils;

namespace FakeNews.Database.Repositories
{
    public interface ITweetRepository
    {
        void Save(IEnumerable<TweetDb> tweetDbs);

        [CacheResult(Duration = 1000)]
        bool IsUserAlreadyInDatabase(long userId);

        [CacheResult(Duration = 1000)]
        bool IsTweetAlreadyInDatabase(long tweetId);
        IEnumerable<TweetDb> GetTweets();
        IEnumerable<UserDb> GetUsers();
        TweetDb GetTweet(long id);

        void SaveChanges();
        void Add(UserDb user);
        void Update(UserDb user);
        void Add(TweetDb tweet);
        void Update(TweetDb tweet);

    }
}