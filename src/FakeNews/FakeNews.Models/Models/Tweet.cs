﻿using Newtonsoft.Json;

namespace FakeNews.TwitterModels.Models
{
    public class Tweet
    {
        public static Tweet FromJson(string json) => JsonConvert.DeserializeObject<Tweet>(json);

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("entities")]
        public Entities Entities { get; set; }

        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("retweet_count")]
        public long? RetweetCount { get; set; }

        [JsonProperty("favorite_count")]
        public long? FavoriteCount { get; set; }

        [JsonProperty("favorited")]
        public bool Favorited { get; set; }

        [JsonProperty("retweeted")]
        public bool Retweeted { get; set; }

        [JsonProperty("possibly_sensitive")]
        public bool PossiblySensitive { get; set; }

        [JsonProperty("lang")]
        public string Lang { get; set; }

        [JsonProperty("score")]
        public decimal? Score { get; set; }
    }
}