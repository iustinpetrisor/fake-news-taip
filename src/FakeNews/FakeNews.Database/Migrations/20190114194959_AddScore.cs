﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FakeNews.Database.Migrations
{
    public partial class AddScore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Score",
                table: "Tweet",
                nullable: false,
                defaultValue: 0.5m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Score",
                table: "Tweet");
        }
    }
}
