import pika
import tensorflow as tf
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
import pickle
import numpy as np
import json

model = load_model('model.h5')

with open('tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)

queueTweets = "queue-tweets"
queueUsers = "queue-users"

queueTweetsClassified = "queue-tweets-classified"
queueUsersClassified = "queue-users-classified"

exchange = "queue-exchange"
exchangeClassified = "queue-exchange-classified"

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

connectionClassified = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channelClassified = connection.channel()

channel.exchange_declare(exchange=exchange,
                         exchange_type='fanout')

channel.queue_declare(queue=queueTweets)

channel.queue_bind(exchange=exchange,
                   queue=queueTweets)

channelClassified.exchange_declare(exchange=exchangeClassified,
                         exchange_type='fanout')

channelClassified.queue_declare(queue=queueTweetsClassified)

channelClassified.queue_bind(exchange=exchangeClassified, queue=queueTweetsClassified)

print(' [*] Waiting for tweets. To exit press CTRL+C')

def callback(ch, method, properties, body):
    print(" [x] %r" % body)
    tweet = json.loads(body)
    text = tweet['text'].replace("RT ", "")
    sequences_test = tokenizer.texts_to_sequences([text])
    x = pad_sequences(sequences_test, maxlen = 10, padding="post", truncating="post")
    score = 0.5
    try:
        y = model.predict(x)
        score = y[0][0]
    except  Exception:
        print("Text has weird characters")
    x = {"tweet": tweet, "score": str(score)} 
    print(str(score))
    channelClassified.basic_publish(exchange=exchangeClassified,
                      routing_key='',
                      body=json.dumps(x))


channel.basic_consume(callback,
                      queue=queueTweets,
                      no_ack=True)

channel.start_consuming()