﻿using System;
using System.Collections.Generic;

namespace FakeNews.Database
{
    public class UserDb
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string ScreenName { get; set; }

        public string Location { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public long? FollowersCount { get; set; }

        public long? FriendsCount { get; set; }

        public long? ListedCount { get; set; }

        public DateTime CreatedAt { get; set; }

        public long? FavouritesCount { get; set; }

        public bool Verified { get; set; }

        public long? StatusesCount { get; set; }

        public string Lang { get; set; }
        public ICollection<TweetDb> TweetDbs { get; set; }
    }
}