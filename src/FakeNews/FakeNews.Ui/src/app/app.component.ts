import { Component, OnInit, ViewChild } from '@angular/core';
import { TweetService } from './services/tweet.service';
import { Tweet } from './shared/models/tweet';
import { Observable } from 'rxjs';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Fake News';
  tweets: Tweet[];
  tweeturi: Observable<Tweet[]>;
  @ViewChild(BaseChartDirective) childCmpBaseChartRef: any;
  public chartLabels:string[] = ['Fake news', 'Real news', 'Uncertain news'];
 
  public chartData:number[] = [];
  public chartType:string = 'doughnut';

  constructor(private tweetService: TweetService) {}

  ngOnInit(): void {
    this.tweeturi =  this.tweetService.getAllTweets();

    this.tweetService.getAllTweets().subscribe(result => {
      this.tweets = result;
      this.chartData[0] = this.tweets.filter(x => x.score < 0.4).length;
      this.chartData[1] = this.tweets.filter(x => x.score >= 0.6).length;
      this.chartData[2] = this.tweets.filter(x => x.score >= 0.4 && x.score < 0.6).length;
      this.childCmpBaseChartRef.ngOnInit();
      console.log(this.chartData);
      
      console.log(this.tweets);
    });
  }
}
