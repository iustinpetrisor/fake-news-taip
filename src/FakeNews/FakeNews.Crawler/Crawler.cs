﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using FakeNews.TwitterModels.Models;

namespace FakeNews.Crawler
{
    public class Crawler
    {
        internal Dictionary<string, string> SearchConfig { get; set; }
        public string ConsumerSecret { get; internal set; }
        public string AccessTokenSecret { get; internal set; }
        public string AccessToken { get; internal set; }
        public string ConsumerKey { get; internal set; }
        public string Url { get; internal set; }


        public Status GetStatus()
        {
            WebRequest request = CreateRequest(
                "GET");

            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        // TODO: uncomment this if you want live data
                        var s = reader.ReadToEnd();
                        //var s = TestJson();
                        return Status.FromJson(s);
                    }
                }
            }
        }

        public string TestJson()
        {
            return File.ReadAllText("testJson.txt");
        }


        public WebRequest CreateRequest(
            string method)
        {
            string encodedParams = EncodeParameters(SearchConfig);
            WebRequest request;
            if (method == "GET")
                request = WebRequest.Create(string.Format("{0}?{1}", Url, encodedParams));
            else
                request = WebRequest.Create(Url);

            request.Method = method;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers.Add(
                "Authorization",
                MakeOAuthHeader(ConsumerKey, ConsumerSecret, AccessToken, AccessTokenSecret, method, Url, SearchConfig));

            if (method == "POST")
            {
                byte[] postBody = new ASCIIEncoding().GetBytes(encodedParams);
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(postBody, 0, postBody.Length);
                }
            }

            return request;
        }

        private static string EncodeParameters(Dictionary<string, string> parameters)
        {
            if (parameters.Count == 0)
                return string.Empty;
            Dictionary<string, string>.KeyCollection.Enumerator keys = parameters.Keys.GetEnumerator();
            keys.MoveNext();
            StringBuilder sb = new StringBuilder(
                string.Format("{0}={1}", keys.Current, Uri.EscapeDataString(parameters[keys.Current])));
            while (keys.MoveNext())
                sb.AppendFormat("&{0}={1}", keys.Current, Uri.EscapeDataString(parameters[keys.Current]));
            keys.Dispose();
            return sb.ToString();
        }

        static string MakeOAuthHeader(string consumerKey, string consumerSecret, string accessToken, string accessKey,
            string method, string url, Dictionary<string, string> parameters)
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            string oauthConsumerKey = consumerKey;
            string oauthNonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            string oauthSignatureMethod = "HMAC-SHA1";
            string oauthToken = accessToken;
            string oauthTimestamp = Convert.ToInt64(ts.TotalSeconds).ToString();
            string oauthVersion = "1.0";

            SortedDictionary<string, string> sd = new SortedDictionary<string, string>();
            if (parameters != null)
                foreach (string key in parameters.Keys)
                    sd.Add(key, Uri.EscapeDataString(parameters[key]));
            sd.Add("oauth_version", oauthVersion);
            sd.Add("oauth_consumer_key", oauthConsumerKey);
            sd.Add("oauth_nonce", oauthNonce);
            sd.Add("oauth_signature_method", oauthSignatureMethod);
            sd.Add("oauth_timestamp", oauthTimestamp);
            sd.Add("oauth_token", oauthToken);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}&{1}&", method, Uri.EscapeDataString(url));
            foreach (KeyValuePair<string, string> entry in sd)
                sb.Append(Uri.EscapeDataString(string.Format("{0}={1}&", entry.Key, entry.Value)));
            string baseString = sb.ToString().Substring(0, sb.Length - 3);

            string oauthTokenSecret = accessKey;
            string signingKey = string.Format(
                "{0}&{1}", Uri.EscapeDataString(consumerSecret), Uri.EscapeDataString(oauthTokenSecret));
            HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
            string oauthSignature =
                Convert.ToBase64String(hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));

            sb = new StringBuilder("OAuth ");
            sb.AppendFormat("oauth_consumer_key=\"{0}\",", Uri.EscapeDataString(oauthConsumerKey));
            sb.AppendFormat("oauth_nonce=\"{0}\",", Uri.EscapeDataString(oauthNonce));
            sb.AppendFormat("oauth_signature=\"{0}\",", Uri.EscapeDataString(oauthSignature));
            sb.AppendFormat("oauth_signature_method=\"{0}\",", Uri.EscapeDataString(oauthSignatureMethod));
            sb.AppendFormat("oauth_timestamp=\"{0}\",", Uri.EscapeDataString(oauthTimestamp));
            sb.AppendFormat("oauth_token=\"{0}\",", Uri.EscapeDataString(oauthToken));
            sb.AppendFormat("oauth_version=\"{0}\"", Uri.EscapeDataString(oauthVersion));

            return sb.ToString();
        }
    }
}