﻿using System;
using System.Collections.Generic;
using System.Text;
using FakeNews.Database.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace FakeNews.Database
{
    public partial class FakeNewsContext : DbContext
    {
        private readonly string _connectionString;
        public virtual DbSet<TweetDb> Tweets { get; set; }
        public virtual DbSet<UserDb> Users { get; set; }

        public FakeNewsContext()
        {
        }

        public FakeNewsContext(DbContextOptions<FakeNewsContext> options)
            : base(options)
        {

        }
        public FakeNewsContext(DbContextOptions<FakeNewsContext> options, string connectionString)
            : base(options)
        {
            _connectionString = connectionString;
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer(_connectionString);
        //    }
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new TweetDbMap());
            modelBuilder.ApplyConfiguration(new UserDbMap());
        }
    }
}
