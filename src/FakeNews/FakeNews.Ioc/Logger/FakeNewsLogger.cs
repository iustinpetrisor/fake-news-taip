﻿using System;
using System.Linq;
using Castle.DynamicProxy;
using Serilog;

namespace FakeNews.Ioc.Logger
{
    public class FakeNewsLogger : IInterceptor, IDisposable
    {
        public FakeNewsLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            Log.Information("Application started");
        }
        public void Intercept(IInvocation invocation)
        {
            var name = $"{invocation.Method.DeclaringType}.{invocation.Method.Name}";
            var args = string.Join(", ", invocation.Arguments.ToList().Select(a => (a ?? "").ToString()));

            Log.Information($"Calling: {name}");
            Log.Information($"Args: {args}");

            var watch = System.Diagnostics.Stopwatch.StartNew();
            invocation.Proceed(); //Intercepted method is executed here.
            watch.Stop();
            var executionTime = watch.ElapsedMilliseconds;

            Log.Information($"Done: result was {invocation.ReturnValue}");
            Log.Information($"Execution Time: {executionTime} ms.");
        }

        public void Dispose()
        {
            Log.Information("Application closed");
            Log.CloseAndFlush();
        }
    }
}