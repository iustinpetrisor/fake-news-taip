using System;
using System.Collections.Generic;
using System.Linq;
using FakeNews.Database;
using FakeNews.Database.Repositories;
using FakeNews.TwitterModels.Models;
using Moq;
using Xunit;

namespace FakeNews.BusinessLogic.Test
{
    public class TweetServiceTest
    {
        private readonly TweetService _tweetService;
        private readonly Mock<ITweetRepository> _tweetRepository = new Mock<ITweetRepository>();
        private readonly IEnumerable<TweetDb> _listTweetDbs = new List<TweetDb>
        {
            new TweetDb
            {
                Id = 3,
                Text = "Test tweet 3"
            },
            new TweetDb
            {
                Id = 4,
                Text = "Test tweet 4"
            },
            new TweetDb
            {
                Id = 5,
                Text = "Test tweet 5"
            }
        };

        public TweetServiceTest()
        {
            _tweetRepository.Setup(x => x.GetTweets()).Returns(_listTweetDbs);
            _tweetRepository.Setup(x => x.GetTweet(It.IsAny<long>())).Returns((long i) => _listTweetDbs.Single(x => x.Id == i));
            _tweetService = new TweetService(_tweetRepository.Object);
        }

        [Fact]
        public void GetReturnsTweets()
        {
            // Arrange
            var service = _tweetService;
            // Act
            var result = service.GetTweets();
            // Assert
            Assert.IsAssignableFrom<IEnumerable<Tweet>>(result);
        }

        [Fact]
        public void ReturnsTweetById()
        {
            // Arrange
            var service = _tweetService;
            // Act
            var result = service.Get(3);

            Assert.NotNull(result); // Test if null
            Assert.IsType<Tweet>(result); // Test type
            Assert.Equal("Test tweet 3", result.Text); // Verify it is the right tweet
        }

        [Fact]
        public void ReturnsAllTweets()
        {
            // Arrange
            var service = _tweetService;
            // Act
            var result = service.GetTweets();
            // Assert
            Assert.IsAssignableFrom<IEnumerable<Tweet>>(result);
            Assert.Equal(3, result.Count()); // Verify the correct Number
        }
    }
}
