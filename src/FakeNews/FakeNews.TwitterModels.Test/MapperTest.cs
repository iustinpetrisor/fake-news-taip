using FakeNews.Database;
using FakeNews.TwitterModels.Mappers;
using FakeNews.TwitterModels.Models;
using Xunit;

namespace FakeNews.TwitterModels.Test
{
    public class MapperTest
    {
        [Fact]
        public void MapTweetReturnsTweetForDatabase()
        {
            var tweet = new Tweet
            {
                Id = 123123123,
                User = new User
                {
                    Id = 148187124981
                }
            };
            TweetDb tweetDatabase = tweet.ToDb();
            Assert.NotEqual(0, tweetDatabase.Id);
        }

        [Fact]
        public void MapTweetHasUserForDatabase()
        {
            var tweet = new Tweet
            {
                Id = 123123123,
                User = new User
                {
                    Id = 148187124981
                }
            };
            TweetDb tweetDatabase = tweet.ToDb();
            Assert.NotNull(tweetDatabase.User);
        }
    }
}
