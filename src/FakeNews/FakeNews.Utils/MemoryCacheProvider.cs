﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace FakeNews.Utils
{
    public class MemoryCacheProvider : ICacheProvider
    {
        private readonly MemoryCache _memoryCache = new MemoryCache(new MemoryCacheOptions
        {
            
        });

        public object Get(string key)
        {
            return _memoryCache.Get(key);
        }

        public void Put(string key, object value, int duration)
        {
            if (duration <= 0)
                throw new ArgumentException("Duration cannot be less or equal to zero", "duration");
            _memoryCache.Set(key, value, DateTime.Now.AddMilliseconds(duration));
        }

        public bool Contains(string key)
        {
            return _memoryCache.Get(key) != null;
        }
    }
}
