﻿using FakeNews.TwitterModels.Models;
using Newtonsoft.Json;

namespace FakeNews.TwitterModels
{
    public static class Serialize
    {
        public static string ToJson(this Tweet self) => JsonConvert.SerializeObject(self);
    }
}