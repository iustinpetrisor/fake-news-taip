﻿using System;
using System.Collections.Generic;
using System.Text;
using FakeNews.TwitterModels;
using FakeNews.TwitterModels.Models;
using FakeNews.Utils;
using RabbitMQ.Client;

namespace FakeNews.Crawler
{
    public static class Queue
    {
        public static void AddToQueue(Status status)
        {
            var factory = new ConnectionFactory() {HostName = "localhost"};
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: QueueConstants.QueueTweets,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                channel.QueueDeclare(queue: QueueConstants.QueueUsers,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);


                channel.ExchangeDeclare(exchange: QueueConstants.Exchange, type: "fanout");

                channel.QueueBind(queue: QueueConstants.QueueUsers,
                    exchange: QueueConstants.Exchange,
                    routingKey: "");
                channel.QueueBind(queue: QueueConstants.QueueTweets,
                    exchange: QueueConstants.Exchange,
                    routingKey: "");



                foreach (var tweet in status.Tweets)
                {
                    var body = Encoding.UTF8.GetBytes(tweet.ToJson());
                    channel.BasicPublish(exchange: QueueConstants.Exchange,
                        routingKey: "",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine(" [x] Sent {0}", body);
                }
                
            }
        }

        private static string GetMessage(string[] args)
        {
            return ((args.Length > 0)
                ? string.Join(" ", args)
                : "info: Hello World!");

        }
    }
}