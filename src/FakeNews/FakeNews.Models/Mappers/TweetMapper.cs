﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using FakeNews.Database;
using FakeNews.TwitterModels.Convertors;
using FakeNews.TwitterModels.Models;

namespace FakeNews.TwitterModels.Mappers
{
    public static class TweetMapper
    {
        public static TweetDb ToDb(this Tweet tweet)
        {
            var tweetDb = new TweetDb
            {
                Id = tweet.Id,
                Text = tweet.Text,
                CreatedAt = tweet.CreatedAt?.ToDateTime() ?? DateTime.MinValue,
                FavoriteCount = tweet.FavoriteCount,
                Favorited = tweet.Favorited,
                Lang = tweet.Lang,
                Hashtags = tweet.Entities != null && tweet.Entities.Hashtags != null ? string.Join(",", tweet.Entities.Hashtags.Select(x => x.Text)) : null,
                // Symbols = tweet.Entities != null ? string.Join(",", tweet.Entities?.Symbols) : null,
                PossiblySensitive = tweet.PossiblySensitive,
                ResultType = tweet.Metadata?.ResultType,
                RetweetCount = tweet.RetweetCount,
                Retweeted = tweet.Retweeted,
                IsoLanguageCode = tweet.Metadata?.IsoLanguageCode,
                Source = tweet.Source,
                User = tweet.User?.ToDb(),
                Score = tweet.Score ?? new decimal(0.5),
                UserId = tweet.User.Id
            };

            return tweetDb;
        }


        public static Tweet ToBusinessLogicModel(this TweetDb tweetDb)
        {
            var tweet = new Tweet
            {
                Id = tweetDb.Id,
                Text = tweetDb.Text,
                CreatedAt = tweetDb.CreatedAt.ToString(),
                FavoriteCount = tweetDb.FavoriteCount,
                Favorited = tweetDb.Favorited,
                Lang = tweetDb.Lang,
                // Symbols = tweetDb.Entities != null ? string.Join(",", tweetDb.Entities?.Symbols) : null,
                PossiblySensitive = tweetDb.PossiblySensitive,
                RetweetCount = tweetDb.RetweetCount,
                Retweeted = tweetDb.Retweeted,
                Source = tweetDb.Source,
                User = tweetDb.User?.ToBusinessLogicModel(),
                Score = tweetDb.Score,
                Entities = new Entities()
                {
                    Hashtags = tweetDb.Hashtags.Split(',').Select(f => new Hashtag { Text = f }).ToArray()
                }
            };

            return tweet;
        }
    }
}
