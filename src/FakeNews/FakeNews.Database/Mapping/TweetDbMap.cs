﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FakeNews.Database.Mapping
{
    public class TweetDbMap : IEntityTypeConfiguration<TweetDb>
    {
        public void Configure(EntityTypeBuilder<TweetDb> builder)
        {
            builder.ToTable("Tweet");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).HasColumnName("Id").ValueGeneratedNever();
            builder.Property(c => c.Text).HasColumnName("Text");
            builder.Property(c => c.Hashtags).HasColumnName("HashTags");
            builder.Property(c => c.Symbols).HasColumnName("Symbols");
            builder.Property(c => c.ResultType).HasColumnName("ResultType");
            builder.Property(c => c.IsoLanguageCode).HasColumnName("IsoLanguageCode");
            builder.Property(c => c.Source).HasColumnName("Source");
            builder.Property(c => c.RetweetCount).HasColumnName("RetweetCount").IsRequired(false);
            builder.Property(c => c.FavoriteCount).HasColumnName("FavoriteCount").IsRequired(false);
            builder.Property(c => c.Favorited).HasColumnName("Favorited");
            builder.Property(c => c.Retweeted).HasColumnName("Retweeted");
            builder.Property(c => c.PossiblySensitive).HasColumnName("PossiblySensitive");
            builder.Property(c => c.Lang).HasColumnName("Lang");
            builder.Property(c => c.Score).HasColumnName("Score");

            builder.Property(c => c.UserId).HasColumnName("UserId").IsRequired();
            builder.HasOne(f => f.User).WithMany(x=> x.TweetDbs).HasForeignKey(f => f.UserId);

        }
    }
}
