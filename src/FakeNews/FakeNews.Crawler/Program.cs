﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;

namespace FakeNews.Crawler
{
    class Program
    {

        static void Main(string[] args)
        {
            
            try
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(
                        Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                IConfigurationRoot configuration = builder.Build();

                var crawlerBuilder = new CrawlerBuilder();
                Crawler crawler = crawlerBuilder.SetConsumerKey(configuration["TwitterCredentials:ConsumerKey"])
                    .SetConsumerSecert(configuration["TwitterCredentials:ConsumerSecret"])
                    .SetAccessToken(configuration["TwitterCredentials:AccessToken"])
                    .SetAccessTokenSecret(configuration["TwitterCredentials:AccessTokenSecret"])
                    .SetSearchConfig(new Dictionary<string, string>() { { "q", "#yellowvests" }, { "lang", "en" } })
                    .SetUrl(configuration["TwitterCredentials:Url"])
                    .Build();

            
                var status = crawler.GetStatus();
                Queue.AddToQueue(status);
                //var tweets1 = service.GetTweets(); 
                //var tweets2 = service.GetTweets();

                //var status = new Status
                //{
                //    Tweets = new Tweet[2] {
                //        new Tweet{
                //            Id = 122312,
                //            Lang = "en",
                //            CreatedAt = "Sun Feb 25 18:11:01 +0000 2018",
                //            User = new User
                //            {
                //                CreatedAt = "Sun Feb 25 18:11:01 +0000 2018",
                //                Id = 62532653
                //            }
                //        },
                //        new Tweet{
                //            Id = 12231,
                //            Lang = "en",
                //            CreatedAt = "Sun Feb 25 18:11:01 +0000 2018",
                //            User = new User
                //            {
                //                CreatedAt = "Sun Feb 25 18:11:01 +0000 2018",
                //                Id = 62532653
                //            }
                //    }
                //}
                //};
                Console.WriteLine("Program end!");

            }

            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey();
        }

        private static string FormatEnvironment(string environment)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(environment);
        }
    }
}
