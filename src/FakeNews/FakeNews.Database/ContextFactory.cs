﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace FakeNews.Database
{
    public class ContextFactory : IDesignTimeDbContextFactory<FakeNewsContext>
    {
        private static IConfigurationRoot Configuration { get; set; }

        public FakeNewsContext CreateDbContext()
        {
            return CreateDbContext(null);
        }

        public FakeNewsContext CreateDbContext(string[] args)
        {
            if (Configuration == null)
            {
                var builderc = new ConfigurationBuilder()
                    .SetBasePath(
                        System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location))
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                var configuration = builderc.Build();
                var optionsBuilder = new DbContextOptionsBuilder<FakeNewsContext>();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
                Configuration = configuration;
            }
            var builder = new DbContextOptionsBuilder<FakeNewsContext>();
            builder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));

            return new FakeNewsContext(builder.Options);
        }
    }
}
