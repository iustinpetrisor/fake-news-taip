# Fake News TAIP


# Team members
* Iustin Petrisor
* Cristian Cristea
* Victor Filimon (Scrum Master)
* Alexandru Mihai

# Coordinator
* Conf. dr. Iftene Adrian

# State of the art
* https://onedrive.live.com/edit.aspx?cid=1439bba9b9a31a97&page=view&resid=1439BBA9B9A31A97!2007&parId=1439BBA9B9A31A97!2006&app=Word
* http://sbp-brims.org/2018/proceedings/papers/challenge_papers/SBP-BRiMS_2018_paper_120.pdf



# Requirements
* https://bitbucket.org/iustinpetrisor/fake-news-taip/src/ca50e6d2ccb6cfe93289334f59283f66a0ba3093/Documentation/?at=master
# Diagrams
* https://bitbucket.org/iustinpetrisor/fake-news-taip/src/master/Documentation/Laborator3/model/laborator3.jpg
# BPMN diagram
* https://bitbucket.org/iustinpetrisor/fake-news-taip/src/master/Documentation/bpmn%20diagram.png?
# Sonar code analysis
* https://bitbucket.org/iustinpetrisor/fake-news-taip/src/master/Documentation/Sonar%20code%20analysis.pdf
# Technical report
* https://bitbucket.org/iustinpetrisor/fake-news-taip/src/40074c9ef96fbbd4551d7b1e39eec57de90ed927/Documentation/Technical%20Report.docx?at=master&fileviewer=file-view-default

# Contact
* victor.filimon@gmail.com