﻿using System.Collections.Generic;

namespace FakeNews.Crawler
{
    public class CrawlerBuilder : ICrawlerBuilder
    {
        private readonly Crawler _crawler = new Crawler();

        public CrawlerBuilder SetConsumerKey(string consumerKey)
        {
            _crawler.ConsumerKey = consumerKey;
            return this;
        }

        public CrawlerBuilder SetSearchConfig(Dictionary<string, string> searchConfig)
        {
            _crawler.SearchConfig = searchConfig;
            return this;
        }

        public CrawlerBuilder SetConsumerSecert(string consumerSecret)
        {
            _crawler.ConsumerSecret = consumerSecret;
            return this;
        }

        public CrawlerBuilder SetAccessToken(string accessToken)
        {
            _crawler.AccessToken = accessToken;
            return this;
        }

        public CrawlerBuilder SetAccessTokenSecret(string accessTokenSecret)
        {
            _crawler.AccessTokenSecret = accessTokenSecret;
            return this;
        }

        public CrawlerBuilder SetUrl(string url)
        {
            _crawler.Url = url;
            return this;
        }

        public Crawler Build()
        {
            return _crawler;
        }
        
    }
}








