﻿using Newtonsoft.Json;

namespace FakeNews.TwitterModels.Models
{
    public class Status
    {
        public static Status FromJson(string json) => JsonConvert.DeserializeObject<Status>(json, Converter.Settings);

        [JsonProperty("statuses")]
        public Tweet[] Tweets { get; set; }
    }
}
