from gensim.models import Word2Vec
import pandas as pd


import glob
path =r'data for invoice' # use your path
allFiles = glob.glob(path + "/*.csv")
frame = pd.DataFrame()
list_ = []
for file_ in allFiles:
    df = pd.read_csv(file_,index_col=None, header=0)
    list_.append(df)
frame = pd.concat(list_)
frame['Name'] = frame['Name'].str.lower()


sentences = []
for i, row in frame.iterrows():
    text = row['Name']
    if row['Class'] == 'Address':
        text = ''.join([i for i in row['Name'] if not i.isdigit()])
        text = " ".join(text.split())
        text = text.replace('.', '')
        text = text.replace(',', '')
        text = text.split(" ")
        text = [word for word in text if len(word) >= 3]
    if row['Class'] == 'Cif':
        text = "".join(text.split())
    if row['Class'] == 'NrReg':
        text = "".join(text.split())
    print(text)

    # print(text, " ++  " , text.split(' '))
    sentences.append(text)
model = Word2Vec(sentences, size=100, window=3, min_count=3, workers=-1)
model.save('model.bin')
print(model)
print((model.wv.vocab)
)

X = frame['Name']



# y is the label for our classification
y =frame['Class']

# from imblearn.over_sampling import RandomOverSampler
# ros = RandomOverSampler(random_state=0)
# X_resampled, y_resampled = ros.fit_resample(X.values.reshape(-1,1), y.values)

X_resampled = X
y_resampled = y
X_resampled = pd.Series(X_resampled.ravel())

eightyPercent = int(len(X_resampled)*0.8)
twentyPercent = int(len(X_resampled)*0.2)
#
from sklearn.preprocessing import LabelEncoder
from keras.utils import np_utils
encoder = LabelEncoder()
encoder.fit(y_resampled)
encoded_Y = encoder.transform(y_resampled)
dummy_y = encoded_Y
#
#
X_train = X_resampled[: eightyPercent]
X_test = X_resampled[eightyPercent:]

y_train = dummy_y[:eightyPercent]
y_test = dummy_y[eightyPercent:]

from keras.layers import Embedding
NUM_WORDS = 5000
EMBEDDING_DIM=100
#
from keras.preprocessing.text import Tokenizer
tokenizer = Tokenizer(num_words=len(model.wv.vocab))
print(X_train)
# Fit the function on the text
tokenizer.fit_on_texts(X_train)

# Count number of unique tokens
word_index = tokenizer.word_index
print('Found %s unique tokens.' % len(word_index))

sequences_train = tokenizer.texts_to_sequences(X_train)
sequences_valid=tokenizer.texts_to_sequences(X_test)
from keras.preprocessing.sequence import pad_sequences
X_train = pad_sequences(sequences_train,maxlen = 10, padding="post", truncating="post")
X_test = pad_sequences(sequences_valid,maxlen = 10, padding="post", truncating="post")


import numpy as np
# Convert target to array
y_train = np.asarray(y_train)
y_test = np.asarray(y_test)
vocabulary_size=min(len(model.wv.vocab),NUM_WORDS)
word_vectors = Word2Vec.load('model.bin')

from keras.layers import Embedding
vocabulary_size=min(len(word_index)+1,NUM_WORDS)

embedding_matrix = np.zeros((vocabulary_size, EMBEDDING_DIM))
for word, i in word_index.items():
    if i>=NUM_WORDS:
        continue
    try:
        embedding_vector = word_vectors[word]
        embedding_matrix[i] = embedding_vector
    except KeyError:
        embedding_matrix[i]=np.random.normal(0,np.sqrt(0.25),EMBEDDING_DIM)

embedding_layer = Embedding(vocabulary_size,
                            EMBEDDING_DIM, weights=[embedding_matrix])

from keras.layers import Input, Dense, Conv2D, MaxPooling2D, Dropout,concatenate
from keras.layers.core import Reshape, Flatten
from keras.callbacks import EarlyStopping
from keras.optimizers import Adam
from keras.models import Model
from keras import regularizers
sequence_length = X_train.shape[1]
print(sequence_length)
filter_sizes = [3,4,5]
num_filters = 10
drop = 0.5
inputs = Input(shape=(sequence_length,))
embedding = embedding_layer(inputs)
reshape = Reshape((sequence_length,EMBEDDING_DIM,1))(embedding)

conv_0 = Conv2D(num_filters, (filter_sizes[0], EMBEDDING_DIM),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)
conv_1 = Conv2D(num_filters, (filter_sizes[1], EMBEDDING_DIM),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)
conv_2 = Conv2D(num_filters, (filter_sizes[2], EMBEDDING_DIM),activation='relu',kernel_regularizer=regularizers.l2(0.01))(reshape)

maxpool_0 = MaxPooling2D((sequence_length - filter_sizes[0] + 1, 1), strides=(1,1))(conv_0)
maxpool_1 = MaxPooling2D((sequence_length - filter_sizes[1] + 1, 1), strides=(1,1))(conv_1)
maxpool_2 = MaxPooling2D((sequence_length - filter_sizes[2] + 1, 1), strides=(1,1))(conv_2)

merged_tensor = concatenate([maxpool_0, maxpool_1, maxpool_2], axis=1)
flatten = Flatten()(merged_tensor)
# reshape = Reshape((3*num_filters,))(flatten)
dropout = Dropout(drop)(flatten)
output = Dense(units=7, activation='softmax',kernel_regularizer=regularizers.l2(0.01))(dropout)

# this creates a model that includes
model = Model(inputs, output)

adam = Adam(lr=1e-3)

model.compile(loss='sparse_categorical_crossentropy',
              optimizer=adam,
              metrics=['acc'])
callbacks = [EarlyStopping(monitor='val_loss')]

model.fit(X_train, y_train, batch_size=100, epochs=20, verbose=1, validation_data=(X_test, y_test))
score = model.evaluate(X_test, y_test, verbose=0)
print('Test accuracy:', score[1])
