﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserProfilerService.Models
{
    public class UserProfile
    {
        public User User { get; set; }
        public bool IsTrusted  { get; set; }
        public double Score { get; set; }
    }
}
