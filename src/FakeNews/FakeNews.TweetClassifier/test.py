
# coding: utf-8

# In[18]:

import tensorflow as tf
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
import pickle
import numpy as np


# In[20]:


model = load_model('model.h5')


# In[21]:


with open('tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)


# In[37]:


sequences_test = tokenizer.texts_to_sequences(['At least 3 killed, 30 injured in blast in Sylhet, Bangladesh'])
x = pad_sequences(sequences_test,maxlen = 10, padding="post", truncating="post")

y = model.predict(x)
print(y)
