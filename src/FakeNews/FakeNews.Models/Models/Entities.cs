﻿using Newtonsoft.Json;

namespace FakeNews.TwitterModels.Models
{
    public class Entities
    {
        [JsonProperty("hashtags")]
        public Hashtag[] Hashtags { get; set; }

        //[JsonProperty("symbols")]
        //public string[] Symbols { get; set; }

    }
}