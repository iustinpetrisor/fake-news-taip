﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FakeNews.Utils;
using Microsoft.EntityFrameworkCore;

namespace FakeNews.Database.Repositories
{
    public class TweetRepository : ITweetRepository
    {
        private readonly FakeNewsContext _fakeNewsContext;
        public TweetRepository()
        {
            var factory = new ContextFactory();
            _fakeNewsContext = factory.CreateDbContext();
        }


        public void Save(IEnumerable<TweetDb> tweetDbs)
        {
            
        }

        public void SaveChanges()
        {
            _fakeNewsContext.SaveChanges();
        }
        public void Add(UserDb user)
        {
            _fakeNewsContext.Users.Add(user);
        }
        public void Update(UserDb user)
        {
            _fakeNewsContext.Users.Update(user);
        }
        public void Add(TweetDb tweet)
        {
            _fakeNewsContext.Tweets.Add(tweet);
        }
        public void Update(TweetDb tweet)
        {
            _fakeNewsContext.Tweets.Update(tweet);
        }

        public bool IsUserAlreadyInDatabase(long userId)
        {
            return _fakeNewsContext.Users.Any(x => x.Id == userId);
        }

        public bool IsTweetAlreadyInDatabase(long tweetId)
        {
            return _fakeNewsContext.Tweets.Any(x => x.Id == tweetId);
        }

        public IEnumerable<TweetDb> GetTweets()
        {
            return _fakeNewsContext.Tweets.Include(f=>f.User).ToList();
        }

        public IEnumerable<UserDb> GetUsers()
        {
            return _fakeNewsContext.Users.ToList();
        }

        public IEnumerable<TweetDb> Get()
        {
            return _fakeNewsContext.Tweets.ToList();
        }

        public TweetDb GetTweet(long id)
        {
            return _fakeNewsContext.Tweets.FirstOrDefault(f=>f.Id == id);
        }
    }
}
