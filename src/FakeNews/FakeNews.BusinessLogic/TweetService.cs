﻿using System.Collections.Generic;
using System.Linq;
using FakeNews.Database;
using FakeNews.Database.Repositories;
using FakeNews.TwitterModels.Mappers;
using FakeNews.TwitterModels.Models;

namespace FakeNews.BusinessLogic
{
    public class TweetService : ITweetService
    {
        private readonly ITweetRepository _tweetRepository;
        public TweetService(ITweetRepository tweetRepository)
        {
            _tweetRepository = tweetRepository;
        }

        public IEnumerable<Tweet> GetTweets()
        {
            return _tweetRepository.GetTweets().Select(x => x.ToBusinessLogicModel());
        }

        public IEnumerable<User> GetUsers()
        {
            return _tweetRepository.GetUsers().Select(x => x.ToBusinessLogicModel());
        }

        public void SaveTweets(IEnumerable<Tweet> tweets)
        {
            var tweetDbs = tweets.Select(x => x.ToDb()).ToList();

            foreach (var tweet in tweetDbs)
            {
                if (!_tweetRepository.IsUserAlreadyInDatabase(tweet.UserId))
                {
                    _tweetRepository.Add(tweet.User);
                }
                tweet.User = null;
                if (!_tweetRepository.IsTweetAlreadyInDatabase(tweet.Id))
                {
                    _tweetRepository.Add(tweet);
                }
                _tweetRepository.SaveChanges();
            }


        }

        public IEnumerable<TweetDb> Get()
        {
            return _tweetRepository.GetTweets();
        }

        public Tweet Get(long id)
        {
            return _tweetRepository.GetTweet(id).ToBusinessLogicModel();
        }
    }
}
