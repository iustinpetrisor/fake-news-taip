﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FakeNews.Database.Mapping
{
    public class UserDbMap : IEntityTypeConfiguration<UserDb>
    {
        public void Configure(EntityTypeBuilder<UserDb> builder)
        {
            builder.ToTable("User");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).HasColumnName("Id").ValueGeneratedNever();

            builder.Property(c => c.Name).HasColumnName("Name");
            builder.Property(c => c.ScreenName).HasColumnName("ScreenName");
            builder.Property(c => c.Location).HasColumnName("Location");
            builder.Property(c => c.Description).HasColumnName("Description");
            builder.Property(c => c.Url).HasColumnName("Url");
            builder.Property(c => c.FollowersCount).HasColumnName("FollowersCount");
            builder.Property(c => c.FriendsCount).HasColumnName("FriendsCount");
            builder.Property(c => c.ListedCount).HasColumnName("ListedCount");
            builder.Property(c => c.CreatedAt).HasColumnName("CreatedAt");
            builder.Property(c => c.FavouritesCount).HasColumnName("FavouritesCount");
            builder.Property(c => c.Verified).HasColumnName("Verified");
            builder.Property(c => c.StatusesCount).HasColumnName("StatusesCount");
            builder.Property(c => c.Lang).HasColumnName("Lang");
        }
    }
}
