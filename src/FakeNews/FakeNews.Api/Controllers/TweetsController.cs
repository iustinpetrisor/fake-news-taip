﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using FakeNews.BusinessLogic;
using FakeNews.Database;
using FakeNews.TwitterModels.Models;
using FakeNews.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace FakeNews.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TweetsController : ControllerBase
    {
        private readonly ITweetService _tweetService;
        public TweetsController(ITweetService tweetService)
        {
            _tweetService = tweetService;
        }
        // GET api/values
        [HttpGet("tweets")]
        public IEnumerable<Tweet> Get()
        {
            try
            {
                return _tweetService.GetTweets();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpGet("queue")]
        public void Queue()
        {
            ListenToQueue();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Tweet Get(long id)
        {
            return _tweetService.Get(id);
        }


        public void ListenToQueue()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: QueueConstants.QueueTweetsClassified,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                channel.ExchangeDeclare(exchange: QueueConstants.ExchangeClassified, type: "fanout");

                channel.QueueBind(queue: QueueConstants.QueueTweetsClassified,
                    exchange: QueueConstants.ExchangeClassified,
                    routingKey: "");

                Console.WriteLine(" [*] Waiting for logs.");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);

                    var classifiedTweet = ClassifiedTweet.FromJson(message);
                    var tweet = classifiedTweet.Tweet;
                    tweet.Score = new decimal(classifiedTweet.Score);
                    _tweetService.SaveTweets(new Tweet[1] { tweet });

                    Console.WriteLine(" [x] {0}", message);
                };
                channel.BasicConsume(queue: QueueConstants.QueueTweetsClassified,
                    autoAck: true,
                    consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }
    }
}
