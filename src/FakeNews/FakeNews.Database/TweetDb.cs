﻿
using System;

namespace FakeNews.Database
{
    public class TweetDb
    {
        public DateTime CreatedAt { get; set; }

        public long Id { get; set; }

        public string Text { get; set; }

        public string Hashtags { get; set; }

        public string Symbols { get; set; }

        public string ResultType { get; set; }

        public string IsoLanguageCode { get; set; }

        public string Source { get; set; }

        public long? RetweetCount { get; set; }

        public long? FavoriteCount { get; set; }

        public bool Favorited { get; set; }

        public bool Retweeted { get; set; }

        public bool PossiblySensitive { get; set; }

        public string Lang { get; set; }

        public UserDb User { get; set; }

        public long UserId { get; set; }

        public decimal Score { get; set; }
    }
}