﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserProfilerService.Models
{
    public class User
    {
        public string Username { get; set; }
        public IEnumerable<Tweet> Tweets { get; set; }
        public IEnumerable<Tweet> Likes { get; set; }
    }
}
