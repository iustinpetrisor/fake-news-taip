﻿using System;

namespace FakeNews.Utils
{
    public class CacheResultAttribute : Attribute
    {
        public int Duration { get; set; }
    }
}
